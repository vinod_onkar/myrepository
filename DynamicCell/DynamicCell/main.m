//
//  main.m
//  DynamicCell
//
//  Created by Mahendra Sonwane on 19/06/14.
//  Copyright (c) 2014 vinod_onkar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
