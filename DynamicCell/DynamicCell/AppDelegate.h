//
//  AppDelegate.h
//  DynamicCell
//
//  Created by Mahendra Sonwane on 19/06/14.
//  Copyright (c) 2014 vinod_onkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
