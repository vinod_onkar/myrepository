//
//  customeCell.m
//  DynamicCell
//
//  Created by Mahendra Sonwane on 19/06/14.
//  Copyright (c) 2014 vinod_onkar. All rights reserved.
//

#import "customeCell.h"

@implementation customeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
