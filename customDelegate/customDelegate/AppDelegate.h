//
//  AppDelegate.h
//  customDelegate
//
//  Created by Mahendra Sonwane on 21/07/14.
//  Copyright (c) 2014 vinod_onkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
