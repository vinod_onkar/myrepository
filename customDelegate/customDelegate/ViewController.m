//
//  ViewController.m
//  customDelegate
//
//  Created by Mahendra Sonwane on 21/07/14.
//  Copyright (c) 2014 vinod_onkar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()<NSURLConnectionDelegate,CLLocationManagerDelegate>
{
	NSMutableData *data;
	NSString *lat,*longi;
	NSString *sourceCoord;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.locManager = [[CLLocationManager alloc]init];
	self.locManager.desiredAccuracy = 50;
	[self.locManager startUpdatingLocation];
	self.locManager.delegate = self;

	data = [[NSMutableData alloc]init];

		// Do any additional setup after loading the view, typically from a nib.
}

-(void)callDistanceMatrixAPIWithSource:(NSString *)sourceCoordinate andDestCoordinates:(NSString *)destCoordinates
{
    //sourceCoordinate = @"19.220312,72.974844";
	destCoordinates = @"19.213616,72.991053";

	NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/distancematrix/json?origins=%@&destinations=%@&mode=driving",sourceCoordinate,destCoordinates];

	NSLog(@"%@", [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    NSURL *url  = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSURLRequest *req = [NSURLRequest requestWithURL:url];

	[NSURLConnection connectionWithRequest:req delegate:self];

}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	data.length = 0;
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data1
{
	[data appendData:data1];



}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
	NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    NSString *distance;

	NSMutableArray *rows = [dict objectForKey:@"rows"];
	NSMutableArray *elements = [[rows objectAtIndex:0]objectForKey:@"elements"];
    NSDictionary *distanceObj = [[elements objectAtIndex:0]objectForKey:@"distance"];
	distance = [distanceObj objectForKey:@"text"];
	NSLog(@"distance:%@",distance);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-locationManager delegates
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"Failed");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation *loc =[locations lastObject];
	destCoordinate =loc.coordinate;
	lat = [NSString stringWithFormat:@"%f",destCoordinate.latitude];
	longi = [NSString stringWithFormat:@"%f",destCoordinate.longitude];

	sourceCoord = [NSString stringWithFormat:@"%@,%@",lat,longi];
	[self callDistanceMatrixAPIWithSource:sourceCoord andDestCoordinates:@""];
	//[manager stopUpdatingLocation];
}

@end
