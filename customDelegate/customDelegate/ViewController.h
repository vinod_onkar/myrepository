//
//  ViewController.h
//  customDelegate
//
//  Created by Mahendra Sonwane on 21/07/14.
//  Copyright (c) 2014 vinod_onkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface ViewController : UIViewController
{
CLLocationCoordinate2D destCoordinate;
}

@property(strong,nonatomic)CLLocationManager *locManager;
@end
