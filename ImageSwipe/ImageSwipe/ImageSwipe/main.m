//
//  main.m
//  ImageSwipe
//
//  Created by Mahendra Sonwane on 24/07/14.
//  Copyright (c) 2014 vinod_onkar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
