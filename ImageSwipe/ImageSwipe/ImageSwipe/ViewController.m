
#import "ViewController.h"

@interface ViewController ()
{
	NSMutableArray *imgArray;
	int i;

	NSMutableArray *imageObjectsArray;
}
@end

@implementation ViewController

#pragma mark-ViewLifeCycle
- (void)viewDidLoad
{
	 i=1;
	[super viewDidLoad];

    imgArray = [[NSMutableArray alloc]initWithObjects:@"images.jpeg",@"images1.jpeg",@"images2.jpeg",@"images3.jpeg", nil];

	imageObjectsArray = [[NSMutableArray alloc]initWithObjects:[UIImage imageNamed:@"images.jpeg"],[UIImage imageNamed:@"images1.jpeg"],[UIImage imageNamed:@"images2.jpeg"],[UIImage imageNamed:@"images3.jpeg"], nil];

	[self.imageView setImage:[UIImage imageNamed:[imgArray objectAtIndex:0]]];

}

-(void)viewDidAppear:(BOOL)animated
{

    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe:)];

	swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;

	UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe:)];

	swipeRight.direction = UISwipeGestureRecognizerDirectionRight;

	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHandle:)];

    [self.imageView addGestureRecognizer:tap];
	[self.imageView addGestureRecognizer:swipeRight];
	[self.imageView addGestureRecognizer:swipeLeft];


    self.imageView.animationImages = imageObjectsArray;
	self.imageView.animationDuration = 2;
	self.imageView.animationRepeatCount = 0;
	[self.imageView startAnimating];


	[super viewDidAppear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark-Gesture events
-(void)handleSwipe:(UISwipeGestureRecognizer *)s
{

    if (s.direction == UISwipeGestureRecognizerDirectionLeft) {

		[UIView animateWithDuration:0.25 animations:^{
			if (i%4 == 0) {
				i = 0;
			}
			[self.imageView setImage:[UIImage imageNamed:[imgArray objectAtIndex:i]]];
			i++;
		}];

	}
	else if(s.direction == UISwipeGestureRecognizerDirectionRight)
	{

		[UIView animateWithDuration:0.25 animations:^{

		if (i==0) {
			i=4;
		}
		i--;
		[self.imageView setImage:[UIImage imageNamed:[imgArray objectAtIndex:i]]];
        }];
	}

}
-(void)tapHandle:(UITapGestureRecognizer *)t
{

	if (CGRectEqualToRect(self.imageView.frame, self.view.bounds)) {
        [UIView animateWithDuration:0.25 animations:^{
		[self.imageView setFrame:CGRectMake(0, 0, 320, 200)];
		}];
	}
	else
	{
		[UIView animateWithDuration:0.25 animations:^{
			[self.imageView setFrame:self.view.bounds];
		}];
	}

}


@end
