
#import "ViewController.h"

@interface ViewController ()
{
	UIImageView *imgView;
	NSTimer *timer;
}
@property (nonatomic, retain) MPMoviePlayerController *mpContrlr;
@property (nonatomic, retain) AVAudioPlayer *audPlayer;
@end

@implementation ViewController
@synthesize mpContrlr,audPlayer;


#pragma mark-ViewLifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];

	[self initVideo];
	[self initAudio];


	[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playOrStop:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:mpContrlr];

	//[mpContrlr play];

}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	[UIView animateWithDuration:0.25 animations:^{
		if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {

			mpContrlr.view.frame = self.view.bounds;
		}
		else
		{
			CGRect rect = self.view.bounds;
			rect.size.height *=  0.5;
			mpContrlr.view.frame = rect;
		}
	}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark-Video Play
-(void)initVideo
{
	mpContrlr = [[MPMoviePlayerController alloc]initWithContentURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"poha" ofType:@"mp4"]]];


	mpContrlr.scalingMode = MPMovieScalingModeAspectFit;
    mpContrlr.movieSourceType = MPMovieSourceTypeFile;

	mpContrlr.view.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;

	CGRect rect = self.view.bounds;
	rect.size.height *=  0.5;
	mpContrlr.view.frame = rect;

	[mpContrlr setShouldAutoplay:NO];

	[self.view addSubview:mpContrlr.view];
	[mpContrlr prepareToPlay];
}

#pragma mark-Audio Play

-(void)initAudio
{
	NSError *error;

	audPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"Lorde" ofType:@"mp3"]] error:&error];

}

#pragma mark- Notifications
-(void)playOrStop:(NSNotification *)noti
{
	if (mpContrlr.playbackState == MPMoviePlaybackStatePlaying) {
		[self play];
	}
	else if (mpContrlr.playbackState == MPMoviePlaybackStatePaused)
	{
		[self Pause];
	}
}

#pragma mark-Movie Player Operations
-(void)play
{
	timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(playBack:) userInfo:nil repeats:YES];

	[audPlayer prepareToPlay];
	[audPlayer play];
}

-(void)Pause
{
	[audPlayer pause];
	[timer invalidate];
}

-(void)playBack:(NSTimer *)t
{
	if (fabs(mpContrlr.currentPlaybackTime - audPlayer.currentTime) > 1) {
		audPlayer.currentTime = mpContrlr.currentPlaybackTime;
	}
}

@end
